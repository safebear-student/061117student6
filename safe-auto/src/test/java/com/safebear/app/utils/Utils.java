package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by CCA_Student on 06/11/2017. ChrisB
 */
public class Utils {

    DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
    private WebDriver driver =  new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"),capabilities);

//    WebDriver driver = new ChromeDriver();
    private String url = "http://automate.safebear.co.uk";

    public Utils() throws MalformedURLException {
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }
}


    /** public boolean navigateToWebsite(WebDriver driver) {
        this.driver = driver;
        url = "http://automate.safebear.co.uk/";
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }
} **/
