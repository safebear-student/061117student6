package com.safebear.app;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by CCA_Student on 06/11/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
            Test01_Login.class,
})
public class TestSuite {
}
